#!/usr/bin/python3
# -*- coding: utf-8 -*-

from db import Data
import cgi
import cgitb
from utils import imprimir_html
import json
import datetime

cgitb.enable()
# print("Content-type:text/html\r\n\r\n")
print("Content-Type: application/json\r\n\r\n")

hbdb = Data("localhost", "cc500273_u", "pretiumrut", "cc500273_db")

form = cgi.FieldStorage()

if not form:
    week = 0
else:
    week = max(0, int(form["offset"].value))

today = datetime.datetime.now()
today = int(today.strftime("%w"))


def offset(x): return (x+today) % 7


myChart = [hbdb.get_count_detalle_avistamiento_on_day(offset(6)+7*week)[0][0],
           hbdb.get_count_detalle_avistamiento_on_day(offset(5)+7*week)[0][0],
           hbdb.get_count_detalle_avistamiento_on_day(offset(4)+7*week)[0][0],
           hbdb.get_count_detalle_avistamiento_on_day(offset(3)+7*week)[0][0],
           hbdb.get_count_detalle_avistamiento_on_day(offset(2)+7*week)[0][0],
           hbdb.get_count_detalle_avistamiento_on_day(offset(1)+7*week)[0][0],
           hbdb.get_count_detalle_avistamiento_on_day(offset(0)+7*week)[0][0]]

myPieChart = [hbdb.get_count_detalle_avistamiento_tipo_insecto()[0][0],
              hbdb.get_count_detalle_avistamiento_tipo_aracnido()[0][0],
              hbdb.get_count_detalle_avistamiento_tipo_miriapodo()[0][0],
              hbdb.get_count_detalle_avistamiento_tipo_nose()[0][0]]

today = datetime.datetime.now()
today = int(today.strftime("%m"))


def offset(x): return (x+today) % 12


myBarChart1 = [hbdb.get_count_detalle_avistamiento_estado_vivo(offset(11))[0][0],
               hbdb.get_count_detalle_avistamiento_estado_vivo(offset(10))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(9))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(8))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(7))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(6))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(5))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(4))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(3))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(2))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(1))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_vivo(offset(0))[0][0]
]

myBarChart2 = [hbdb.get_count_detalle_avistamiento_estado_muerto(offset(11))[0][0],
               hbdb.get_count_detalle_avistamiento_estado_muerto(offset(10))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(9))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(8))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(7))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(6))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(5))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(4))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(3))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(2))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(1))[0][0],
    hbdb.get_count_detalle_avistamiento_estado_muerto(offset(0))[0][0]]

myBarChart3 = [hbdb.get_count_detalle_avistamiento_estado_nose(offset(11))[0][0],
               hbdb.get_count_detalle_avistamiento_estado_nose(offset(10))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(9))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(8))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(7))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(6))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(5))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(4))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(3))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(2))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(1))[
    0][0],
    hbdb.get_count_detalle_avistamiento_estado_nose(offset(0))[0][0]]

x = {
    "myChart": myChart,
    "myPieChart": myPieChart,
    "myBarChart1": myBarChart1,
    "myBarChart2": myBarChart2,
    "myBarChart3": myBarChart3
}
print(json.dumps(x))
