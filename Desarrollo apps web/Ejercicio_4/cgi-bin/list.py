#!/usr/bin/python3
# -*- coding: utf-8 -*-
from db import Doctor
import cgi
import cgitb
from utils import imprimir_html
import html

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

hbdb = Doctor("localhost", "cc500273_u", "pretiumrut", "cc500273_db")
data = hbdb.get_doctors()


found = """
<table>
    <tr>
        <th>Nombre</th>
        <th>Experiencia</th>
        <th>Especialidad </th>
        <th>Email contacto </th>
        <th>Celular contacto </th>
        <th>Foto médico </th>
    </tr>
"""

if data:
    for d in data:
        found += "<tr>"
        for i in range(1, len(d) - 1):
            found += f"<th>{html.escape(str(d[i]))}</th>"

        foto = hbdb.get_archivo_from_id(d[0])
        found += f'<th><img src="../img/{html.escape(str(foto[0][1]))}" style="width:120px;height:120px;"></th>'
        found += "</tr>"
    found += "</table>"

    imprimir_html(found)

else:
    imprimir_html("<h1> No hay médicos registrados</h1>")
