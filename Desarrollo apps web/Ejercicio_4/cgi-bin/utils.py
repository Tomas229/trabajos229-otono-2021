utf8stdout = open(1, 'w', encoding='utf-8', closefd=False)


def imprimir_html(msg):
    with open('../template.html', 'r') as f:
        s = f.read()
        print(s.format(msg), file=utf8stdout)


def imprimir_error(msg):
    msg_html = f"<h1>{msg}</h1>"
    with open('../template.html', 'r') as f:
        s = f.read()
        print(s.format(msg_html), file=utf8stdout)
    exit()
