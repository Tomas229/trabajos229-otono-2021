function modalOpenChangeImage(src) {
    document.getElementById("modal-image").src = src;
    $('#imageModal').modal('toggle');
}

function goBack() {
    window.history.back();
}