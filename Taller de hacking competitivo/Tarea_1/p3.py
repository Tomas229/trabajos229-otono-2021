
PUBLIC_EXPONENT = 3  # It is faster!


def decode_from_int(msg_int):
    '''
    transforms an integer into a message.
    '''
    msg_chr = ""
    while msg_int != 0:
        c = chr(int(msg_int % 256))
        msg_chr = c + msg_chr
        msg_int >>= 8
    return msg_chr


def decrypt(message, exponent):
    '''
    Decrypts an int message using a given public exponent.
    '''
    msg_int = pow(message, 1/exponent)
    msg = decode_from_int(int(msg_int))
    return msg


if __name__ == "__main__":
    decrypted = []
    flag_maybe = None

    with open("encrypted.txt") as in_file:
        for i, line in enumerate(in_file):
            if i == 2294:
                flag_maybe = line
                continue
            dec = decrypt(int(line, 16), PUBLIC_EXPONENT)
            decrypted.append(dec[:6]+'\n')

    with open("decrypted.txt", "w") as out_file:
        out_file.writelines(flag_maybe)
        out_file.writelines(decrypted)

    print(decode_from_int(
        22888253089534818649112153841442447766653))
# Primera parte línea problemática
# 0x4a4b85eb4eb2294496fe6e12431b1696dffeb8b06f87731f5b403888b8b8042bd73c6efe99d327022503554634eefa4d64965
# Segunda parte línea problemática
# 0xe1cea2cb658895b5ad1d21c143f43c893c141aa00b716975151a7368b53e7725a8489590a2bc6372efd709ca21cbade11ec54c20cb5dbe17c186210f09e018b4ad2f82cca70c7e8
