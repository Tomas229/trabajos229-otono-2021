#!/usr/bin/python3
# -*- coding: utf-8 -*-


from db import Doctor
import cgi
import cgitb
from utils import imprimir_html

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

form = cgi.FieldStorage()
hbdb = Doctor("localhost", "cc500273_u", "pretiumrut", "cc500273_db")

data = (form["nombre-medico"].value, form["experiencia-medico"].value,
        form["especialidad-medico"].value, form["email-medico"].value,
        form["celular-medico"].value, form["foto-medico"])

hbdb.save_doctor(data)

imprimir_html("<h1> Los datos fueron enviados con éxito </h1>")
