#!/usr/bin/python3
# -*- coding: utf-8 -*-

from db import Data
import cgi
import cgitb
from utils import imprimir_html
import html

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

hbdb = Data("localhost", "cc500273_u", "pretiumrut", "cc500273_db")

form = cgi.FieldStorage()
data_avistamiento = hbdb.get_avistamiento_from_id(form["id"].value)


content = ""
scripts = '<script src="../js/data.js"></script>'
head = '<link rel="stylesheet" href="../CSS/bichoStyle_data.css">'
if data_avistamiento:
    data_avistamiento = data_avistamiento[0]
    content += """
        <div class="border border-success rounded main container">
        <div class="row">
            <div class="text-center col mb-5">
                <b>Datos de contacto:</b>
            </div>
        </div>
        <div class="row">
            <div class="text-center col-sm">
                <b>Nombre:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(data_avistamiento[1])) + """
            </div>
            <div class="text-center col-sm">
                <b>Email:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(data_avistamiento[2])) + """
            </div>
            <div class="text-center col-sm">
                <b>Celular:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(data_avistamiento[3])) + """
            </div>
        </div>
        <hr class="my-4">
        <div class="row">
            <div class="text-center col mb-5">
                <b>Lugar:</b>
            </div>
        </div>
        <div class="row">
            <div class="text-center col-sm">
                <b>Región:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(data_avistamiento[4])) + """
            </div>
            <div class="text-center col-sm">
                <b>Comuna:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(data_avistamiento[5])) + """
            </div>
            <div class="text-center col-sm">
                <b>Sector:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(data_avistamiento[6])) + """
            </div>
        </div>
        <hr class="my-4">
    """

    data_detalle_avistamiento = hbdb.get_detalle_avistamiento_from_av_id(
        data_avistamiento[0])

    for da in data_detalle_avistamiento:
        content += """
        <div class="row">
            <div class="text-center col mb-5">
                <b>Avistamiento:</b>
            </div>
        </div>
        <div class="row">
            <div class="text-center col-sm">
                <b>Día-Hora:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(da[1])) + """
            </div>
            <div class="text-center col-sm">
                <b>Tipo:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(da[2])) + """
            </div>
            <div class="text-center col-sm">
                <b>Estado:</b>
            </div>
            <div class="text-center col">
                """ + html.escape(str(da[3])) + """
            </div>
        </div>
        """
        fotos = hbdb.get_foto_from_da_id(da[0])
        for fo in fotos:
            content += """
            <div class="text-center col mt-5">
                <img src="../img/""" + html.escape(str(fo[0])) + """ " class="data-image-small" onclick="modalOpenChangeImage('../img/""" + html.escape(str(fo[0])) + """ ');"
                    alt="Imagen detalle avistamiento">
            </div>
            """
        content += '<hr class="my-4">'

    content += """
    <button type="button" class="btn-lg btn-secondary btn-block text-center" onclick="goBack()">
        Volver
    </button>
    
    <a class="btn-lg btn-primary btn-block text-center" href="index.py" role="button">Ir al menú</a>

    <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="img/ladybug.jpg" class="data-image-big" id="modal-image" alt="Imagen chinita">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    """

    imprimir_html(
        content, scripts, head)

else:
    content += """
    <h1 class= "text-center">No se encontraron datos</h1>
    <button type="button" class="btn-lg btn-secondary btn-block text-center" onclick="goBack()">
        Volver
    </button>
    """
    imprimir_html(content, scripts, head)
