#!/usr/bin/python3
# -*- coding: utf-8 -*-

import mysql.connector
import hashlib
import os
import filetype
from utils import imprimir_error
import re
from datetime import datetime, timedelta


MAX_FILE_SIZE = 10000 * 1000  # 10 MB


class Data:

    def __init__(self, host, user, password, database):
        self.db = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        self.cursor = self.db.cursor()

    def imprimir_error(self, text):
        self.db.rollback()
        imprimir_error(text)
        return -1

    def save_avistamiento(self, region, comuna, sector, nombre, email, celular):
        now = datetime.now()
        dia_hora = now.strftime("%Y-%m-%d %H:%M")

        # Los datos están bien, se envían a la base de datos
        sql = '''
           INSERT INTO avistamiento (comuna_id, dia_hora, sector, nombre, email, celular)
           VALUES (%s, %s, %s, %s, %s, %s)
        '''
        self.cursor.execute(sql, (comuna, dia_hora, sector, nombre,
                                  email, celular))  # ejecuto la consulta con el nuevo avistamiento
        return self.cursor.getlastrowid()  # id del avistamiento creado

    def verify_avistamiento(self, region, comuna, sector, nombre, email, celular):
        error = ""
        # Región y Comuna
        try:
            region = int(region)
            comuna = int(comuna)
        except:
            error += "<p>La Comuna y/o región tienen un formato inválido</p>"

        sql = """
         SELECT region_id FROM comuna WHERE comuna.id=%s
         """
        self.cursor.execute(sql, (comuna,))
        val = self.cursor.fetchall()

        if not val:
            error += "<p>La información de la comuna no es consistente con la base de datos</p>"
        if val[0][0] != region:
            error += "<p>La información de la región no es consistente con la comuna</p>"

        if(region == 0):
            error += "<p>Se debe seleccionar una región</p>"
        if(comuna == 0):
            error += "<p>Se debe seleccionar una comuna</p>"

        # Sector
        if(len(sector) > 200):
            error += "<p>El texto de sector no puede superar los 200 carácteres</p>"

        # Nombre
        if(nombre == ""):
            error += "<p>Se debe escribir un nombre</p>"

        if(len(nombre) > 100):
            error += "<p>El nombre no puede superar los 100 carácteres</p>"

        if not re.search("^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*$", nombre):
            error += "<p>El nombre solamente puede estar compuesto por letras</p>"

        # Email

        if(email == ""):
            error += "<p>Se debe escribir un email</p>"

        if(len(email) > 100):
            error += "<p>El email no puede superar los 100 carácteres</p>"

        if not re.search("""(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""", email):
            error += "<p>El email tiene un formato inválido</p>"

        # Celular

        if(len(celular) > 15):
            error += "<p>El celular no puede superar los 15 carácteres</p>"

        if not re.search("|^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$", celular):
            error += "<p>El celular tiene un formato inválido</p>"

        return error

    def save_detalle_avistamiento(self, dia_hora, tipo, estado, avistamiendo_id):
        # Los datos están bien, se envían a la base de datos
        sql = """
            INSERT INTO detalle_avistamiento (dia_hora, tipo, estado, avistamiento_id)
            VALUES (%s, %s, %s, %s)
        """

        self.cursor.execute(sql, (dia_hora, tipo, estado,
                                  avistamiendo_id))  # ejecuto la consulta con el nuevo detalle_avistamiento
        return self.cursor.getlastrowid()  # id del detalle_avistamiento

    def verify_detalle_avistamiento(self, dia_hora, tipo, estado):
        error = ""
        # Dia Hora
        if(dia_hora == ""):
            error += "<p>Se debe escribir un día y hora para el avistamiento</p>"

        if not re.search("[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]", dia_hora):
            error += "<p>El día y hora no tiene formato aaaa-mm-dd hh:mm</p>"

        # Tipo
        if tipo not in ['no sé', 'insecto', 'arácnido', 'miriápodo']:
            error += "<p>Se debe seleccionar un tipo entre las opciones</p>"

        # Estado
        if estado not in ['no sé', 'vivo', 'muerto']:
            error += "<p>Se debe seleccionar un estado entre las opciones</p>"

        return error

    def save_foto(self, foto, detalle_avistamiento_id):
        filename = foto.filename
        # Largo ruta
        # calculamos cuantos elementos existen y actualizamos el hash
        sql = "SELECT COUNT(id) FROM foto"
        self.cursor.execute(sql)
        total = self.cursor.fetchall()[0][0] + 1  # peligroso TODO mejorar
        hash_archivo = str(total) + \
            hashlib.sha256(filename.encode()).hexdigest()[0:30]

        # Guardar el archivo
        file_path = '../../../img/' + hash_archivo

        try:
            f = open(file_path, 'wb')
        except:
            self.imprimir_error(
                "Hubo un problema con los permisos al abrir el destino, contacta con soporte")
            return -1
            # Esto ocurre cuando olvido dar los permisos correspondientes, por eso lo trato de forma distinta, ya que no debería ocurrir.
        try:
            f.write(foto.file.read())
        except:
            self.imprimir_error(
                "Hubo un problema con los permisos al subir el archivo, contacta con soporte")
            return -2
            # Esto ocurre cuando olvido dar los permisos correspondientes, por eso lo trato de forma distinta, ya que no debería ocurrir.
        finally:
            f.close()

        # verificamos el tipo, si no es valido lo borramos de la db
        tipo = filetype.guess(file_path)
        if tipo.mime.split("/")[0] != 'image':
            os.remove(file_path)
            return False

        sql = """
            INSERT INTO foto (ruta_archivo, nombre_archivo, detalle_avistamiento_id)
            VALUES (%s, %s, %s)
        """

        self.cursor.execute(
            sql, (hash_archivo, filename, detalle_avistamiento_id))  # ejecuto la consulta con la nuevo foto
        return file_path

    def verify_foto(self, foto):
        error = ""
        # Verificamos el tamaño   TODO tamaño mínimo
        try:
            size = os.fstat(foto.file.fileno()).st_size
            if size > MAX_FILE_SIZE:
                erorr += '<p>Tamaño excede 10mb</p>'
        except:
            error += "<p>Hubo un problema con el tamaño del archivo</p>"  # TODO averiguar

        return error

    def fotos_list_2_detalles_avistamiento(self, list_foto, detalle_avistamiento_id):
        # Se agregan la fotos válidas asociadas con el detalle avistamiento y se obtiene su id
        id_list = []
        for foto in list_foto:
            if foto.filename:
                id_list += [self.save_foto(foto, detalle_avistamiento_id)]
        return id_list

    def verify_fotos_list_2_detalles_avistamiento(self, list_foto):
        error = ""
        if len(list_foto) > 5:
            error += '<p>Se admite un máximo de cinco fotos por detalle de avistamiento</p>'

        if len(list(filter(lambda x: x.filename, list_foto))) < 1:
            error += '<p>Se debe ingresar al menos una foto por detalle de avistamiento</p>'

        return error

    def save_data(self, region, comuna, sector, nombre, email, celular, list_dia_hora, list_tipo, list_estado, list_foto, list_num_foto):

        # Pre processing
        if not isinstance(list_dia_hora, list):
            list_dia_hora = [list_dia_hora]

        if not isinstance(list_tipo, list):
            list_tipo = [list_tipo]

        if not isinstance(list_estado, list):
            list_estado = [list_estado]

        if not isinstance(list_foto, list):
            list_foto = [list_foto]

        if not isinstance(list_num_foto, list):
            list_num_foto = [list_num_foto]

        for i in range(len(list_num_foto)):
            list_num_foto[i] = int(list_num_foto[i].value)

        # Lista que tiene una lista de fotos correspondiente a cada detalle avistamiento
        list_detalles_foto = []
        for _ in range(len(list_dia_hora)):
            list_detalles_foto += [[]]

        for i in range(len(list_num_foto)):
            list_detalles_foto[list_num_foto[i]] += [list_foto[i]]

        # Validations
        error = ""
        error += self.verify_avistamiento(region,
                                          comuna, sector, nombre, email, celular)

        if((len(list_dia_hora) != len(list_tipo)) or (len(list_dia_hora) != len(list_estado))):
            error += "<p>Inconsistencia con la cantidad de campos por detalle avistamiento</p>"

        for i in range(len(list_dia_hora)):
            error += self.verify_detalle_avistamiento(
                list_dia_hora[i].value, list_tipo[i].value, list_estado[i].value)

        for i in list_detalles_foto:
            error += self.verify_fotos_list_2_detalles_avistamiento(i)

        for foto in list_foto:
            if foto.filename:
                error += self.verify_foto(foto)

        if error:
            return error

        # Populate database
        id_avistamiento = self.save_avistamiento(
            region, comuna, sector, nombre, email, celular)

        id_detalles_list = []

        for i in range(len(list_dia_hora)):
            id_detalles_list += [self.save_detalle_avistamiento(
                list_dia_hora[i].value, list_tipo[i].value, list_estado[i].value, id_avistamiento)]

        file_path_fotos_list = []

        for i in range(len(list_detalles_foto)):
            file_path_fotos_list += [self.fotos_list_2_detalles_avistamiento(
                list_detalles_foto[i], id_detalles_list[i])]

        if False in file_path_fotos_list:
            for fp in file_path_fotos_list:
                if fp:
                    os.remove(fp)

            self.db.rollback()
            return "<p>Uno de los archivos no era una imagen</p>"

        self.db.commit()  # Todo ok, se sube todo a la base de datos

        return False

    def get_index(self):

        sql = """
        SELECT DA.id, DA.dia_hora, CO.nombre, AV.sector, DA.tipo
        FROM avistamiento AV, detalle_avistamiento DA, comuna CO
        WHERE DA.avistamiento_id = AV.id AND AV.comuna_id=CO.id
        ORDER BY DA.dia_hora DESC
        LIMIT 5
        """
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def get_list(self, offset):
        sql = """
        SELECT AV.id, AV.dia_hora, CO.nombre, AV.sector, AV.nombre
        FROM avistamiento AV, comuna CO
        WHERE AV.comuna_id=CO.id
        ORDER BY AV.dia_hora DESC
        LIMIT %s, 5
        """
        self.cursor.execute(sql, (offset,))
        return self.cursor.fetchall()

    def get_list_count_fo(self, id):
        sql = """
        SELECT COUNT(FO.id)
        FROM avistamiento AV, detalle_avistamiento DA, foto FO
        WHERE DA.avistamiento_id = AV.id AND FO.detalle_avistamiento_id = DA.id AND AV.id = %s
        """
        self.cursor.execute(sql, (id,))
        return self.cursor.fetchall()

    def get_list_count_da(self, id):
        sql = """
        SELECT COUNT(DA.id)
        FROM avistamiento AV, detalle_avistamiento DA
        WHERE DA.avistamiento_id = AV.id AND AV.id = %s
        """
        self.cursor.execute(sql, (id,))
        return self.cursor.fetchall()

    def get_av_count(self):
        sql = """
        SELECT COUNT(*)
        FROM avistamiento AV
        """
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def get_index_foto(self, id):
        sql = """
        SELECT ruta_archivo FROM foto WHERE detalle_avistamiento_id = %s LIMIT 1
        """
        self.cursor.execute(sql, (id,))
        return self.cursor.fetchall()

    def get_avistamiento_from_id(self, id):
        sql = """
        SELECT AV.id, AV.nombre, AV.email, AV.celular, CO.nombre, RE.nombre, AV.sector
        FROM avistamiento AV, comuna CO, region RE
        WHERE AV.comuna_id=CO.id AND CO.region_id = RE.id AND AV.id = %s
        """
        self.cursor.execute(sql, (id,))
        return self.cursor.fetchall()

    def get_detalle_avistamiento_from_av_id(self, id):
        sql = """
        SELECT DA.id, DA.dia_hora, DA.tipo, DA.estado
        FROM detalle_avistamiento DA
        WHERE DA.avistamiento_id = %s
        """
        self.cursor.execute(sql, (id,))
        return self.cursor.fetchall()

    def get_foto_from_da_id(self, id):
        sql = """
        SELECT ruta_archivo
        FROM foto
        WHERE detalle_avistamiento_id = %s
        """
        self.cursor.execute(sql, (id,))
        return self.cursor.fetchall()
        pass

    def get_count_detalle_avistamiento_on_day(self, offset):
        day = datetime.now() - timedelta(days=offset)
        next_day = day + timedelta(days=1)

        day = day.strftime("%Y-%m-%d")
        next_day = next_day.strftime("%Y-%m-%d")

        sql = """
        SELECT COUNT(id) FROM detalle_avistamiento WHERE dia_hora BETWEEN %s AND %s
        """
        self.cursor.execute(sql, (day, next_day))
        return self.cursor.fetchall()

    def get_count_detalle_avistamiento_tipo_insecto(self):
        sql = """
        SELECT COUNT(id) FROM detalle_avistamiento WHERE tipo = "insecto"
        """
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def get_count_detalle_avistamiento_tipo_aracnido(self):
        sql = """
        SELECT COUNT(id) FROM detalle_avistamiento WHERE tipo = "arácnido"
        """
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def get_count_detalle_avistamiento_tipo_miriapodo(self):
        sql = """
        SELECT COUNT(id) FROM detalle_avistamiento WHERE tipo = "miriápodo"
        """
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def get_count_detalle_avistamiento_tipo_nose(self):
        sql = """
        SELECT COUNT(id) FROM detalle_avistamiento WHERE tipo = "no sé"
        """
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def get_count_detalle_avistamiento_estado_vivo(self, offset):
        day = datetime.now()
        for _ in range(offset):
            day = day.replace(day=1)
            day = day - timedelta(days=1)
        day = day.strftime("%Y-%m-%d")

        sql = """
        SELECT COUNT(*) FROM detalle_avistamiento 
        WHERE MONTH(dia_hora) = MONTH(%s) 
        AND YEAR(dia_hora) = YEAR(%s) 
        AND estado = "vivo"
        """
        self.cursor.execute(sql, (day, day))
        return self.cursor.fetchall()

    def get_count_detalle_avistamiento_estado_muerto(self, offset):
        day = datetime.now()
        for _ in range(offset):
            day = day.replace(day=1)
            day = day - timedelta(days=1)
        day = day.strftime("%Y-%m-%d")

        sql = """
        SELECT COUNT(*) FROM detalle_avistamiento 
        WHERE MONTH(dia_hora) = MONTH(%s) 
        AND YEAR(dia_hora) = YEAR(%s) 
        AND estado = "muerto"
        """
        self.cursor.execute(sql, (day, day))
        return self.cursor.fetchall()

    def get_count_detalle_avistamiento_estado_nose(self, offset):
        day = datetime.now()
        for _ in range(offset):
            day = day.replace(day=1)
            day = day - timedelta(days=1)
        day = day.strftime("%Y-%m-%d")

        sql = """
        SELECT COUNT(*) FROM detalle_avistamiento 
        WHERE MONTH(dia_hora) = MONTH(%s) 
        AND YEAR(dia_hora) = YEAR(%s) 
        AND estado = "no sé"
        """
        self.cursor.execute(sql, (day, day))
        return self.cursor.fetchall()

    def get_count_by_comuna(self):
        sql = """
        SELECT CO.nombre, COUNT(*)
        FROM  avistamiento AV, comuna CO, detalle_avistamiento DA, foto FO
        WHERE AV.comuna_id = CO.id AND DA.avistamiento_id = AV.id AND FO.detalle_avistamiento_id = DA.id
        GROUP BY comuna_id
        """
        self.cursor.execute(sql)
        return self.cursor.fetchall()

    def get_avistamiento_from_comuna(self, comuna):

        sql = """
        SELECT DA.id, DA.dia_hora, DA.tipo, DA.estado, AV.id
        FROM detalle_avistamiento DA, avistamiento AV, comuna CO
        WHERE DA.avistamiento_id = AV.id AND AV.comuna_id = CO.id AND CO.nombre = %s
        """
        self.cursor.execute(sql, (comuna,))
        return self.cursor.fetchall()
