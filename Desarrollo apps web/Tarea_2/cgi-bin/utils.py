utf8stdout = open(1, 'w', encoding='utf-8', closefd=False)


def imprimir_html(msg, footer="", header=""):
    with open('../template.html', 'r') as f:
        s = f.read()
        print(s.format(msg, footer, header), file=utf8stdout)


def imprimir_form(errors=""):
    footer = """
    <script src="../js/reportar.js"></script>
    <script>document.getElementsByName("dia-hora-avistamiento")[0].value = getDateFormat();</script>
    <script>noConstraint();</script>
    """
    with open("../form.html", 'r') as f:
        body = f.read()
        imprimir_html(body.format(errors), footer)


def imprimir_error(msg):
    msg_html = f"<h1>{msg}</h1>"
    with open('../template.html', 'r') as f:
        s = f.read()
        print(s.format(msg_html, "", ""), file=utf8stdout)
    exit()
