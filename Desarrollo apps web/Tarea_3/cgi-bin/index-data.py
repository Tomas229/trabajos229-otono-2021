#!/usr/bin/python3
# -*- coding: utf-8 -*-

from db import Data
import cgi
import cgitb
from utils import imprimir_html
import json
import datetime
import html

cgitb.enable()
print("Content-Type: application/json\r\n\r\n")


hbdb = Data("localhost", "cc500273_u", "pretiumrut", "cc500273_db")


def format_img_from_da_id(id):
    payload = "<tr>"
    fotos = hbdb.get_foto_from_da_id(id)

    len_fotos = len(fotos)

    if(len_fotos < 4):
        for i in range(len_fotos):
            payload += f'<td><img src="https://anakena.dcc.uchile.cl/~tcortez/img/{html.escape(str(fotos[i][0]))}" class="img-fluid" alt="Responsive image"></td>'
        payload += "".join(["<td></td>", "<td></td>",
                            "<td></td>", "<td></td>"][:-(i+1)])
    if(len_fotos == 4):
        for i in range(2):
            payload += f'<td><img src="https://anakena.dcc.uchile.cl/~tcortez/img/{html.escape(str(fotos[i][0]))}" class="img-fluid" alt="Responsive image"></td>'
        payload += "<td></td><td></td>"
        payload += "</tr><tr>"

        for i in range(2, len_fotos):
            payload += f'<td><img src="https://anakena.dcc.uchile.cl/~tcortez/img/{html.escape(str(fotos[i][0]))}" class="img-fluid" alt="Responsive image"></td>'

    if(len_fotos > 4):
        for i in range(3):
            payload += f'<td><img src="https://anakena.dcc.uchile.cl/~tcortez/img/{html.escape(str(fotos[i][0]))}" class="img-fluid" alt="Responsive image"></td>'
        payload += "<td></td>"
        payload += "</tr><tr>"

        for i in range(3, len_fotos):
            payload += f'<td><img src="https://anakena.dcc.uchile.cl/~tcortez/img/{html.escape(str(fotos[i][0]))}" class="img-fluid" alt="Responsive image"></td>'
        payload += "<td></td><td></td>"
    payload += "</tr>"
    return payload


x = dict()

for par in hbdb.get_count_by_comuna():
    nombre = par[0]
    payload = """
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Fecha-Hora</th>
                <th scope="col">Tipo</th>
                <th scope="col">Estado</th>
                <th scope="col">Más info</th>
            </tr>
        </thead>
        <tbody>
    """
    data = hbdb.get_avistamiento_from_comuna(nombre)

    for av in data:
        payload += "<tr>"
        for i in range(1, len(av)-1):
            payload += f"<td>{html.escape(str(av[i]))}</td>"

        payload += f"""
            <td>
                <a href="data.py?id={av[-1]}" target="_blank" rel="noopener noreferrer">
                    Ver avistamiento
                </a>
            </td>
        </tr>
        """
        payload += format_img_from_da_id(av[0])

    payload += """
        </tbody>
    </table>
    """
    x.update({nombre: [par[1], payload]})

print(json.dumps(x))
