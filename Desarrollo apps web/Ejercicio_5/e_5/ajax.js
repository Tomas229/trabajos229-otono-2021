function showError(msg) {

    let contenedor = document.getElementById('error');

    contenedor.innerHTML = msg;
    contenedor.style.display = 'block';
    contenedor.style.fontWeight = '800';
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    document.body.scrollTop = 0; // For Safari

}


function sendForm() {
    //Validations
    let msgError = '';

    let nombre = document.getElementById('nombre-medico').value;
    let experiencia = document.getElementById('experiencia-medico').value;
    let especialidad = document.getElementById('especialidad-medico').value;
    let email = document.getElementById('email-medico').value;
    let celular = document.getElementById('celular-medico').value;


    let regex = /^[a-zA-Z ]+$/;
    if (nombre.length > 20 || !regex.test(nombre)) {
        msgError += 'Nombre ';
    }


    if (experiencia.length < 1 || experiencia.length > 1000) {
        msgError += "Experiencia "
    }


    if (especialidad.length < 1 || especialidad.length > 20) {
        msgError += "Especialidad "
    }


    if (email.length < 1 || email.length > 100) {
        msgError += "Email "
    }

    if (celular.length < 1 || celular.length > 100) {
        msgError += "Celular "
    }

    // Errors view
    if (msgError.length > 0) {
        showError('Tu formulario fallo en: ' + msgError);
        return false;
    }

    //Submit
    let data = new FormData();
    data.append('nombre-medico', nombre);
    data.append('experiencia-medico', experiencia);
    data.append('especialidad-medico', especialidad);
    data.append('email-medico', email);
    data.append('celular-medico', celular);
    data.append('foto-medico', document.getElementById('foto-medico').files[0]);


    let xhr = new XMLHttpRequest();
    xhr.open('POST', '../cgi-bin/e5_save_doctor.py', true);
    xhr.timeout = 3000;
    xhr.onload = function (data) {
        alert('Enviado correctamente!');
        document.getElementById('error').style.display = "None"
    };
    xhr.onerror = function () {
        showError('No se pudo enviar el mensaje');
    }

    xhr.send(data);
    return false;

}

function readData() {
    console.log('Cargando mensajes desde el servidor');
    let data = new FormData();
    let xhr = new XMLHttpRequest();
    xhr.open('GET', '../cgi-bin/e5_list.py', true)
    xhr.timeout = 3000;
    xhr.onload = function (data) {
        document.getElementById('list').innerHTML = data.currentTarget.responseText;
    };
    xhr.onerror = function () {
        console.warn('No se pudo enviar el mensaje');
    }

    xhr.send(data);
}

function list() {
    readData();
    setInterval(function () {
        readData();
    }, 1000);
}