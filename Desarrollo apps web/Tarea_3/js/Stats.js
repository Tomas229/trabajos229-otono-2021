var myChartData = [0, 0, 0, 0, 0, 0, 0, 0];
var myPieChartData = [0, 0, 0, 0];
var myBarChart1 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var myBarChart2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
var myBarChart3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

var ctx1 = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx1, {
    type: 'line',
    data: {
        labels: ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'],
        datasets: [{
            label: 'Avitamientos por día',
            data: myChartData,
            fill: false,
            borderColor: 'rgb(75, 192, 192)',
            tension: 0.1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        },
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Detalle avistamiento de la última semana'
            }
        }
    }
});

var ctx2 = document.getElementById('PieChart').getContext('2d');
var myPieChart = new Chart(ctx2, {
    type: 'pie',
    data: {
        labels: ['Insecto', 'Arácnido', 'Miriápodo', 'No sé'],
        datasets: [{
            label: 'Tipos de avistamientos',
            data: myPieChartData,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            hoverOffset: 4
        }]
    },
    options: {
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Detalle avistamiento histórico'
            }
        }
    }
});

var ctx3 = document.getElementById('BarChart').getContext('2d');
var myBarChart = new Chart(ctx3, {
    type: 'bar',
    data: {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        datasets: [{
            label: 'Seres vivos',
            data: myBarChart1,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)'
            ],
            borderWidth: 1
        }
            ,
        {
            label: 'Seres muertos',
            data: myBarChart2,
            backgroundColor: [
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(54, 162, 235, 1)'
            ],
            borderWidth: 1
        }
            ,
        {
            label: 'No sé',
            data: myBarChart3,
            backgroundColor: [
                'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }


        ]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        },
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Detalle avistamiento del último año'
            }
        }
    }
});


var offset = 0



function updateMyChart(data) {
    var chart = myChart
    for (let i = 0; i < data.length; i++) {
        chart.data.datasets[0].data[i] = data[i]
    };
    chart.update();;
}

function updateMyPieChart(data) {
    var chart = myPieChart
    for (let i = 0; i < data.length; i++) {
        chart.data.datasets[0].data[i] = data[i]
    };
    chart.update();
}

function updateMyBarChart(data1, data2, data3) {
    var chart = myBarChart

    for (let i = 0; i < data1.length; i++) {
        chart.data.datasets[0].data[i] = data1[i]
    };
    for (let i = 0; i < data2.length; i++) {
        chart.data.datasets[1].data[i] = data2[i]
    };
    for (let i = 0; i < data3.length; i++) {
        chart.data.datasets[2].data[i] = data3[i]
    };
    chart.update();
}


function readData(offset) {
    console.log('Cargando mensajes desde el servidor');
    let data = new FormData();
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'cgi-bin/stats.py?offset=' + String(offset), true)
    xhr.timeout = 3000;
    xhr.onload = function (data) {
        console.log(data);
        d = JSON.parse(data.currentTarget.response);
        updateMyChart(d["myChart"]);
        updateMyPieChart(d["myPieChart"]);
        updateMyBarChart(d["myBarChart1"], d["myBarChart2"], d["myBarChart3"]);
    };
    xhr.onerror = function () {
        console.warn('No se pudo enviar el mensaje');
    }

    xhr.send(data);
}

function next() {
    if (offset > 0) {
        offset = offset - 1;
    } else {
        alert("Mostrando semana actual");
    }
    console.log(offset);
    updateChart();
}

function prev() {
    offset = offset + 1;
    console.log(offset);
    updateChart();
}

function updateChart() {
    readData(offset);
    if (offset > 0) {
        myChart.options.plugins.title.text = 'Detalle avistamiento ' + String(offset) + 'semana atrás';
    } else {
        myChart.options.plugins.title.text = 'Detalle avistamiento de la última semana';
    }
    myChart.update();
}
readData(0);