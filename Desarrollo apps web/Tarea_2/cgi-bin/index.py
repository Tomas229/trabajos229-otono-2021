#!/usr/bin/python3
# -*- coding: utf-8 -*-

from db import Data
import cgi
import cgitb
from utils import imprimir_html
import html

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

hbdb = Data("localhost", "cc500273_u", "pretiumrut", "cc500273_db")
form = cgi.FieldStorage()
data = hbdb.get_index()
success = ""

if form:
    if form["success"].value == "True":
        success += """
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            Hemos recibido tu información.
            Gracias por colaborar
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>"""

success += """
    <div class="jumbotron jumbotron-fluid main-jumbo">
        <div class="container">
            <h1 class="display-4 text-center">Bienvenido a Bicho-Go!</h1>
            <p class="lead text-center">¡Comparte tus avistamientos de bichos al mundo!</p>
        </div>
    </div>
"""

if data:
    success += """
    <div class="border border-success rounded main">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Fecha-Hora</th>
                    <th scope="col">Comuna</th>
                    <th scope="col">Sector</th>
                    <th scope="col">Tipo</th>
                    <th scope="col">Foto</th>
                </tr>
            </thead>
            <tbody>
    """
    for d in data:
        success += "<tr>"
        for i in range(1, len(d)):
            success += f"<td>{html.escape(str(d[i]))}</td>"

        foto = hbdb.get_index_foto(d[0])[0][0]
        success += f'<td><img src="../img/{html.escape(str(foto))}" class = "data-image-small" alt = "Imagen de avistamiento"></td>'
        success += "</tr>"

    success += """
            </tbody>
        </table>
    </div>
    """

    imprimir_html(success)

else:
    success += '<h1 class= "text-center">No se encontraron datos</h1>'
    imprimir_html(success)
