#!/usr/bin/python3
# -*- coding: utf-8 -*-


import cgi
import cgitb

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

utf8stdout = open(1, 'w', encoding='utf-8', closefd=False)

head = """
<!DOCTYPE html>
<html lang="es">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8" /> <!-- Declaring enconding as UTF 8-->
    <title> Ejercicio 3</title> <!-- Title in pestaña -->
    <link rel="stylesheet" type="text/css" media="screen"  href="../style.css" />    <!-- CSS: -->
</head>
"""
body = f"""
<body>

<ul class="topnav">
  <li><a class="active" href="../index.html">Inicio</a></li>
  <li><a href="../add_doctor.html">Agregar Datos de Médico</a></li>
</ul>


<h1> Los datos fueron enviados con éxito </h1>
"""


footer = f"""
</body >

"""
form = cgi.FieldStorage()
print(head, file=utf8stdout)

print(body, file=utf8stdout)

print("<p><b>Nombre: </b>" +
      form["nombre-medico"].value + "</p>", file=utf8stdout)
print("<p><b>Especialidad: </b>" +
      form["especialidad-medico"].value + "</p>", file=utf8stdout)
print("<p><b>Experiencia: </b>" +
      form["experiencia-medico"].value + "</p>", file=utf8stdout)
print("<hr/> <b>Contacto:</b>", file=utf8stdout)
print("<p><b>E-mail: </b>" +
      form["email-medico"].value + "</p>", file=utf8stdout)
print("<p><b>Celular: </b>" +
      form["celular-medico"].value + "</p>", file=utf8stdout)


print(footer, file=utf8stdout)
