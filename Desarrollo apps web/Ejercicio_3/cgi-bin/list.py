#!/usr/bin/python3
# -*- coding: utf-8 -*-
from db import Doctor
import cgi
import cgitb

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

utf8stdout = open(1, 'w', encoding='utf-8', closefd=False)

head = """
<!DOCTYPE html>
<html lang="es">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8" /> <!-- Declaring enconding as UTF 8-->
    <title> Ejercicio 3</title> <!-- Title in pestaña -->
    <link rel="stylesheet" type="text/css" media="screen"  href="../style.css" />    <!-- CSS: -->
</head>
"""
body = """
<body>

<ul class="topnav">
  <li><a class="active" href="../index.html">Inicio</a></li>
  <li><a href="../add_doctor.html">Agregar Datos de Médico</a></li>
</ul>
"""

found = """
<table>
    <tr>
        <th>Nombre</th>
        <th>Experiencia</th>
        <th>Especialidad </th>
        <th>Email contacto </th>
        <th>Celular contacto </th>
    </tr>
"""

not_found = """
<h1> No hay médicos registrados</h1>
"""

footer = f"""
</body >

"""

hbdb = Doctor("localhost", "cc500273_u", "pretiumrut", "cc500273_db")
data = hbdb.get_doctors()
print(head, file=utf8stdout)
print(body, file=utf8stdout)

if data:  # No sé mi panaaaaaaaaaaaaaaaaa
    for d in data:
        found += "<tr>"
        for i in range(1, len(d)):
            found += f"<th>{str(d[i])}</th>"
        found += "</tr>"
    found += "</table>"
    print(found, file=utf8stdout)
else:
    print(not_found, file=utf8stdout)


print(footer, file=utf8stdout)
