#!/usr/bin/python3
# -*- coding: utf-8 -*-


from e5_db import Doctor
import cgi
import cgitb

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

form = cgi.FieldStorage()
hbdb = Doctor("localhost", "cc500273_u", "pretiumrut", "cc500273_db")

data = (form["nombre-medico"].value, form["experiencia-medico"].value,
        form["especialidad-medico"].value, form["email-medico"].value,
        form["celular-medico"].value, form["foto-medico"])

hbdb.save_doctor(data)

print("<h1> Los datos fueron enviados con éxito </h1>")
