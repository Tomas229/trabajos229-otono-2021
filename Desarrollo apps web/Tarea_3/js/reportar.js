function errorRegion() {
    document.getElementsByName("region")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("region")[0].value;
    let textError = "";
    if (text == "0") {
        document.getElementsByName("region")[0].style.borderColor = "red";
        textError += "<p>Se debe seleccionar una región</p>";
    }
    return textError;
}

function errorComuna() {
    document.getElementsByName("comuna")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("comuna")[0].value;
    let textError = "";
    if (text == "0") {
        document.getElementsByName("comuna")[0].style.borderColor = "red";
        textError += "<p>Se debe seleccionar una comuna</p>";
    }
    return textError;
}


function errorSector() {
    document.getElementsByName("sector")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("sector")[0].value;
    let textError = "";
    if (text.length > 200) {
        document.getElementsByName("sector")[0].style.borderColor = "red";
        textError += "<p>El texto de sector no puede superar los 200 carácteres</p>";
    }
    return textError;
}

function errorNombre() {
    document.getElementsByName("nombre")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("nombre")[0].value;
    let textError = "";
    if (text == "") {
        document.getElementsByName("nombre")[0].style.borderColor = "red";
        textError += "<p>Se debe escribir un nombre</p>";
    }
    if (text.length > 100) {
        document.getElementsByName("nombre")[0].style.borderColor = "red";
        textError += "<p>El nombre no puede superar los 100 carácteres</p>";
    }
    let regex = /^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*$/; //aceptar ñ y acentos
    if (!regex.test(text)) {
        document.getElementsByName("nombre")[0].style.borderColor = "red";
        textError += "<p>El nombre solamente puede estar compuesto por letras</p>";
    }
    return textError;
}

function errorEmail() {
    document.getElementsByName("email")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("email")[0].value;
    let textError = "";
    if (text == "") {
        document.getElementsByName("email")[0].style.borderColor = "red";
        textError += "<p>Se debe escribir un email</p>";
    }
    let regex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    if (!regex.test(text)) {
        document.getElementsByName("email")[0].style.borderColor = "red";
        textError += "<p>El email tiene un formato inválido</p>";
    }
    return textError;
}

function errorCelular() {
    document.getElementsByName("celular")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("celular")[0].value;
    let textError = "";
    let regex = /|^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
    if (!regex.test(text)) {
        document.getElementsByName("celular")[0].style.borderColor = "red";
        textError += "<p>El celular tiene un formato inválido</p>";
    }
    return textError;
}

function errorDiaHora() {
    let textError = "";
    let listDiaHora = document.getElementsByName("dia-hora-avistamiento");
    listDiaHora.forEach(function (currentValue, currentIndex) {
        currentValue.style.borderColor = "#ced4da"
        if (currentValue.value === "") {
            currentValue.style.borderColor = "red";
            textError += "<p>Se debe escribir un día y hora para el avistamiento " + String(currentIndex + 1) + " </p>";
        } else {
            let regex = /[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]/;
            if (!regex.test(currentValue.value)) {
                currentValue.style.borderColor = "red";
                textError += "<p>El día y hora del avistamiento " + String(currentIndex + 1) + "no tiene formato aaaa-mm-dd hh:mm</p>";
            }
        }
    }
    );
    return textError;
}

function errorTipo() {
    let textError = "";
    let listTipo = document.getElementsByName("tipo-avistamiento");
    listTipo.forEach(function (currentValue, currentIndex) {
        currentValue.style.borderColor = "#ced4da"
        if (currentValue.value == "0") {
            currentValue.style.borderColor = "red";
            textError += "<p>Se debe seleccionar un tipo para el avistamiento " + String(currentIndex + 1) + " </p>";
        }
    }
    );
    return textError;
}

function errorEstado() {
    let textError = "";
    let listEstado = document.getElementsByName("estado-avistamiento");
    listEstado.forEach(function (currentValue, currentIndex) {
        currentValue.style.borderColor = "#ced4da"
        if (currentValue.value == "0") {
            currentValue.style.borderColor = "red";
            textError += "<p>Se debe seleccionar un estado para el avistamiento " + String(currentIndex + 1) + " </p>";
        }
    }
    );
    return textError;
}

function errorFoto() {
    let images = document.getElementsByName("foto-avistamiento");
    let textError = "";

    let list = [[]];
    let index = 0;
    images.forEach(
        function (currentValue) {
            let id = currentValue.parentNode.parentNode.id;
            id = id[id.length - 1];
            if (index != id) {
                index += 1;
                list.push([]);
            }
            list[index].push(currentValue.value);
        }
    );

    //Verify if there's at least one file uploaded
    for (j = 0; j < list.length; j++) {
        atLeastOne = false;
        for (h = 0; h < list[j].length; h++) {
            if (list[j][h] != "") {
                atLeastOne = true;
            }
        }
        if (!atLeastOne) {
            textError += "<p>Se debe seleccionar al menos una foto del avistamiento " + String(j + 1) + "</p>";
        } else {
            //If there is at least one file, verify if it's an image
            let regex = /(\.jpg|\.jpeg|\.png)$/;
            for (h = 0; h < list[j].length; h++) {
                if (list[j][h] != "" && !regex.test(list[j][h])) {
                    textError += "<p>El archivo subido número " + String(h + 1) + " del avistamiento " + String(j + 1) + " no es una imagen</p>"
                }
            }
        }
    }
    return textError
}

function errorVerifier() {
    let textError = ""
    textError += errorRegion();
    textError += errorComuna();
    textError += errorSector();
    textError += errorNombre();
    textError += errorEmail();
    textError += errorCelular();
    textError += errorDiaHora();
    textError += errorTipo();
    textError += errorEstado();
    textError += errorFoto();

    let alertMessage = document.getElementById("userErrors");
    if (textError !== "") {
        alertMessage.innerHTML = textError;
        alertMessage.style.display = "block";
        $('#sendModal').modal('hide');
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        document.body.scrollTop = 0; // For Safari
        return false;
    }
    return true;
}

function getDateFormat() {
    let date = new Date();
    let year = date.getFullYear();
    let month = String(date.getMonth() + 1).padStart(2, '0');
    let day = String(date.getDate()).padStart(2, '0');
    let hour = String(date.getHours() + 1).padStart(2, '0');
    let minute = String(date.getMinutes() + 1).padStart(2, '0');

    return year + '-' + month + '-' + day + ' ' + hour + ':' + minute
}

function anotherFile(n) {
    let number = 0
    document.getElementsByName("foto-avistamiento").forEach(
        function (currentValue) {
            if (currentValue.parentNode.parentNode.id == "image-buttons" + String(n)) {
                number += 1;
            }
        }
    );
    if (number < 5) {
        let c = document.getElementById("image-buttons" + String(n));
        c.insertAdjacentHTML("beforeend", '<div class="col-sm-6"><input type = "file" class="form-control-file" name = "foto-avistamiento"> </div><input name="numfoto" type="hidden" value="' + String(n) + '">');
    } else {
        alert("No se pueden subir más fotos");
    }
}

let numPhotos = 0;
function anotherReport() {
    numPhotos += 1;
    let c = document.getElementById("info-avistamiento");
    c.insertAdjacentHTML("beforeend",
        `
    <div class="text-center">
    <b>Información avistamiento `+ String(numPhotos + 1) + `:</b>
    </div>

    <div class="form-group row">
        <label for="inputFecha" class="col-sm-2 col-form-label">Día hora:</label>
        <div class="col-sm-10">
            <input type="text" size="20" class="form-control" name="dia-hora-avistamiento"
                placeholder="aaaa-mm-dd hh:mm">
        </div>
    </div>

    <div class="form-group row">
        <label for="inputTipo" class="col-sm-2 col-form-label">Tipo:</label>
        <div class="col-sm-10">
            <select class="custom-select" name="tipo-avistamiento">
                <option selected value="0">Seleccione el tipo del avistamiento</option>
                <option value="no sé">No sé</option>
                <option value="insecto">Insecto</option>
                <option value="arácnido">Arácnido</option>
                <option value="miriápodo">Miriápodo</option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEstado" class="col-sm-2 col-form-label">Estado:</label>
        <div class="col-sm-10">
            <select class="custom-select" name="estado-avistamiento">
                <option selected value="0">Seleccione un estado para el avistamiento</option>
                <option value="no sé">No sé</option>
                <option value="vivo">Vivo</option>
                <option value="muerto">Muerto</option>
            </select>
        </div>
    </div>

    <input name="numfoto" type="hidden" value="`+ String(numPhotos) + ` ">
    ` + `
    <div class="form-group row" id="image-buttons`+ String(numPhotos) + `">
        <label for="inputFoto" class="col-sm-2 col-form-label">Foto:</label>
        <div class="col-sm-6">
            <input type="file" class="form-control-file" name="foto-avistamiento">
        </div>
        <div class="col-sm-4">
            <button class="btn-success text-center" role="button" type="button" onclick="anotherFile(`+ String(numPhotos) + `)">
                                Agregar otra foto
            </button>
        </div>
    </div>
    <hr class="my-4">
    `);
    let diaHoraForm = document.getElementsByName("dia-hora-avistamiento")
    diaHoraForm[diaHoraForm.length - 1].value = getDateFormat();
}

function noConstraint() {
    document.getElementsByName("sector")[0].removeAttribute('maxLength');

    document.getElementsByName("nombre")[0].required = false;
    document.getElementsByName("nombre")[0].removeAttribute('maxLength');

    document.getElementsByName("email")[0].required = false;

    // for
    document.getElementsByName("dia-hora-avistamiento").forEach(function (currentValue) {
        currentValue.required = false;
    });
}


var RegionesYcomunas = {
    "regiones": [{
        "NombreRegion": "Región de Tarapacá",
        "comunas": ["Camiña", "Huara", "Pozo Almonte", "Iquique", "Pica", "Colchane", "Alto Hospicio"],
        "ValorRegion": 1,
        "ValorComunas": [10301, 10302, 10303, 10304, 10305, 10306, 10307]
    },
    {
        "NombreRegion": "Región de Antofagasta",
        "comunas": ["Tocopilla", "Maria Elena", "Ollague", "Calama", "San Pedro Atacama", "Sierra Gorda", "Mejillones", "Antofagasta", "Taltal"],
        "ValorRegion": 2,
        "ValorComunas": [20101, 20102, 20201, 20202, 20203, 20301, 20302, 20303, 20304]

    },
    {
        "NombreRegion": "Región de Atacama",
        "comunas": ["Diego de Almagro", "Chañaral", "Caldera", "Copiapo", "Tierra Amarilla", "Huasco", "Freirina", "Vallenar", "Alto del Carmen"],
        "ValorRegion": 3,
        "ValorComunas": [30101, 30102, 30201, 30202, 30203, 30301, 30302, 30303, 30304]
    },
    {
        "NombreRegion": "Región de Coquimbo",
        "comunas": ["La Higuera", "La Serena", "Vicuña", "Paihuano", "Coquimbo", "Andacollo", "Rio Hurtado", 'Ovalle', 'Monte Patria', 'Punitaqui', 'Combarbala', 'Mincha', 'Illapel', 'Salamanca', 'Los Vilos'],
        "ValorRegion": 4,
        "ValorComunas": [40101, 40102, 40103, 40104, 40105, 40106, 40201, 40202, 40203, 40204, 40205, 40301, 40302, 40303, 40304]
    },
    {
        "NombreRegion": "Región de Valparaíso",
        "comunas": ['Petorca'
            , 'Cabildo'
            , 'Papudo'
            , 'La Ligua'
            , 'Zapallar'
            , 'Putaendo'
            , 'Santa Maria'
            , 'San Felipe'
            , 'Pencahue'
            , 'Catemu'
            , 'Llay Llay'
            , 'Nogales'
            , 'La Calera'
            , 'Hijuelas'
            , 'La Cruz'
            , 'Quillota'
            , 'Olmue'
            , 'Limache'
            , 'Los Andes'
            , 'Rinconada'
            , 'Calle Larga'
            , 'San Esteban'
            , 'Puchuncavi'
            , 'Quintero'
            , 'Viña del Mar'
            , 'Villa Alemana'
            , 'Quilpue'
            , 'Valparaiso'
            , 'Juan Fernandez'
            , 'Casablanca'
            , 'Concon'
            , 'Isla de Pascua'
            , 'Algarrobo'
            , 'El Quisco'
            , 'El Tabo'
            , 'Cartagena'
            , 'San Antonio'
            , 'Santo Domingo'],
        "ValorRegion": 5,
        "ValorComunas": [50101,
            50102,
            50103,
            50104,
            50105,
            50201,
            50202,
            50203,
            50204,
            50205,
            50206,
            50301,
            50302,
            50303,
            50304,
            50305,
            50306,
            50307,
            50401,
            50402,
            50403,
            50404,
            50501,
            50502,
            50503,
            50504,
            50505,
            50506,
            50507,
            50508,
            50509,
            50601,
            50701,
            50702,
            50703,
            50704,
            50705,
            50706]
    },
    {
        "NombreRegion": "Región del Libertador Bernardo Ohiggins",
        "comunas": ['Mostazal'
            , 'Codegua'
            , 'Graneros'
            , 'Machali'
            , 'Rancagua'
            , 'Olivar'
            , 'Doñihue'
            , 'Requinoa'
            , 'Coinco'
            , 'Coltauco'
            , 'Quinta Tilcoco'
            , 'Las Cabras'
            , 'Rengo'
            , 'Peumo'
            , 'Pichidegua'
            , 'Malloa'
            , 'San Vicente'
            , 'Navidad'
            , 'La Estrella'
            , 'Marchigue'
            , 'Pichilemu'
            , 'Litueche'
            , 'Paredones'
            , 'San Fernando'
            , 'Peralillo'
            , 'Placilla'
            , 'Chimbarongo'
            , 'Palmilla'
            , 'Nancagua'
            , 'Santa Cruz'
            , 'Pumanque'
            , 'Chepica'
            , 'Lolol'],
        "ValorRegion": 6,
        "ValorComunas": [60101,
            60102,
            60103,
            60104,
            60105,
            60106,
            60107,
            60108,
            60109,
            60110,
            60111,
            60112,
            60113,
            60114,
            60115,
            60116,
            60117,
            60201,
            60202,
            60203,
            60204,
            60205,
            60206,
            60301,
            60302,
            60303,
            60304,
            60305,
            60306,
            60307,
            60308,
            60309,
            60310]
    },
    {
        "NombreRegion": "Región del Maule",
        "comunas": ['Teno'
            , 'Romeral'
            , 'Rauco'
            , 'Curico'
            , 'Sagrada Familia'
            , 'Hualañe'
            , 'Vichuquen'
            , 'Molina'
            , 'Licanten'
            , 'Rio Claro'
            , 'Curepto'
            , 'Pelarco'
            , 'Talca'
            , 'Pencahue'
            , 'San Clemente'
            , 'Constitucion'
            , 'Maule'
            , 'Empedrado'
            , 'San Rafael'
            , 'San Javier'
            , 'Colbun'
            , 'Villa Alegre'
            , 'Yerbas Buenas'
            , 'Linares'
            , 'Longavi'
            , 'Retiro'
            , 'Parral'
            , 'Chanco'
            , 'Pelluhue'
            , 'Cauquenes'],
        "ValorRegion": 7,
        "ValorComunas": [70101,
            70102,
            70103,
            70104,
            70105,
            70106,
            70107,
            70108,
            70109,
            70201,
            70202,
            70203,
            70204,
            70205,
            70206,
            70207,
            70208,
            70209,
            70210,
            70301,
            70302,
            70303,
            70304,
            70305,
            70306,
            70307,
            70308,
            70401,
            70402,
            70403]
    },
    {
        "NombreRegion": "Región del Biobío",
        "comunas": ['Tome'
            , 'Florida'
            , 'Penco'
            , 'Talcahuano'
            , 'Concepcion'
            , 'Hualqui'
            , 'Coronel'
            , 'Lota'
            , 'Santa Juana'
            , 'Chiguayante'
            , 'San Pedro de la Paz'
            , 'Hualpen'
            , 'Cabrero'
            , 'Yumbel'
            , 'Tucapel'
            , 'Antuco'
            , 'San Rosendo'
            , 'Laja'
            , 'Quilleco'
            , 'Los Angeles'
            , 'Nacimiento'
            , 'Negrete'
            , 'Santa Barbara'
            , 'Quilaco'
            , 'Mulchen'
            , 'Alto Bio Bio'
            , 'Arauco'
            , 'Curanilahue'
            , 'Los Alamos'
            , 'Lebu'
            , 'Cañete'
            , 'Contulmo'
            , 'Tirua'],
        "ValorRegion": 8,
        "ValorComunas": [80201,
            80202,
            80203,
            80204,
            80205,
            80206,
            80207,
            80208,
            80209,
            80210,
            80211,
            80212,
            80301,
            80302,
            80303,
            80304,
            80305,
            80306,
            80307,
            80308,
            80309,
            80310,
            80311,
            80312,
            80313,
            80314,
            80401,
            80402,
            80403,
            80404,
            80405,
            80406,
            80407]
    },
    {
        "NombreRegion": "Región de La Araucanía",
        "comunas": ['Renaico'
            , 'Angol'
            , 'Collipulli'
            , 'Los Sauces'
            , 'Puren'
            , 'Ercilla'
            , 'Lumaco'
            , 'Victoria'
            , 'Traiguen'
            , 'Curacautin'
            , 'Lonquimay'
            , 'Perquenco'
            , 'Galvarino'
            , 'Lautaro'
            , 'Vilcun'
            , 'Temuco'
            , 'Carahue'
            , 'Melipeuco'
            , 'Nueva Imperial'
            , 'Puerto Saavedra'
            , 'Cunco'
            , 'Freire'
            , 'Pitrufquen'
            , 'Teodoro Schmidt'
            , 'Gorbea'
            , 'Pucon'
            , 'Villarrica'
            , 'Tolten'
            , 'Curarrehue'
            , 'Loncoche'
            , 'Padre Las Casas'
            , 'Cholchol'],
        "ValorRegion": 9,
        "ValorComunas": [90101,
            90102,
            90103,
            90104,
            90105,
            90106,
            90107,
            90108,
            90109,
            90110,
            90111,
            90201,
            90202,
            90203,
            90204,
            90205,
            90206,
            90207,
            90208,
            90209,
            90210,
            90211,
            90212,
            90213,
            90214,
            90215,
            90216,
            90217,
            90218,
            90219,
            90220,
            90221]
    },
    {
        "NombreRegion": "Región de Los Lagos",
        "comunas": ['San Pablo'
            , 'San Juan'
            , 'Osorno'
            , 'Puyehue'
            , 'Rio Negro'
            , 'Purranque'
            , 'Puerto Octay'
            , 'Frutillar'
            , 'Fresia'
            , 'Llanquihue'
            , 'Puerto Varas'
            , 'Los Muermos'
            , 'Puerto Montt'
            , 'Maullin'
            , 'Calbuco'
            , 'Cochamo'
            , 'Ancud'
            , 'Quemchi'
            , 'Dalcahue'
            , 'Curaco de Velez'
            , 'Castro'
            , 'Chonchi'
            , 'Queilen'
            , 'Quellon'
            , 'Quinchao'
            , 'Puqueldon'
            , 'Chaiten'
            , 'Futaleufu'
            , 'Palena'
            , 'Hualaihue'],
        "ValorRegion": 10,
        "ValorComunas": [100201,
            100202,
            100203,
            100204,
            100205,
            100206,
            100207,
            100301,
            100302,
            100303,
            100304,
            100305,
            100306,
            100307,
            100308,
            100309,
            100401,
            100402,
            100403,
            100404,
            100405,
            100406,
            100407,
            100408,
            100409,
            100410,
            100501,
            100502,
            100503,
            100504]
    },
    {
        "NombreRegion": "Región Aisén del General Carlos Ibáñez del Campo",
        "comunas": ['Guaitecas'
            , 'Cisnes'
            , 'Aysen'
            , 'Coyhaique'
            , 'Lago Verde'
            , 'Rio Iba?ez'
            , 'Chile Chico'
            , 'Cochrane'
            , 'Tortel'
            , "O''Higins"],
        "ValorRegion": 11,
        "ValorComunas": [110101,
            110102,
            110103,
            110201,
            110202,
            110301,
            110302,
            110401,
            110402,
            110403]
    },
    {
        "NombreRegion": "Región de Magallanes y la Antártica Chilena",
        "comunas": ['Torres del Paine'
            , 'Puerto Natales'
            , 'Laguna Blanca'
            , 'San Gregorio'
            , 'Rio Verde'
            , 'Punta Arenas'
            , 'Porvenir'
            , 'Primavera'
            , 'Timaukel'
            , 'Antartica'],
        "ValorRegion": 12,
        "ValorComunas": [120101,
            120102,
            120201,
            120202,
            120203,
            120204,
            120301,
            120302,
            120303,
            120401,]
    },
    {
        "NombreRegion": "Región Metropolitana de Santiago",
        "comunas": ['Tiltil'
            , 'Colina'
            , 'Lampa'
            , 'Conchali'
            , 'Quilicura'
            , 'Renca'
            , 'Las Condes'
            , 'Pudahuel'
            , 'Quinta Normal'
            , 'Providencia'
            , 'Santiago'
            , 'La Reina'
            , 'Ñuñoa'
            , 'San Miguel'
            , 'Maipu'
            , 'La Cisterna'
            , 'La Florida'
            , 'La Granja'
            , 'Independencia'
            , 'Huechuraba'
            , 'Recoleta'
            , 'Vitacura'
            , 'Lo Barrenechea'
            , 'Macul'
            , 'Peñalolen'
            , 'San Joaquin'
            , 'La Pintana'
            , 'San Ramon'
            , 'El Bosque'
            , 'Pedro Aguirre Cerda'
            , 'Lo Espejo'
            , 'Estacion Central'
            , 'Cerrillos'
            , 'Lo Prado'
            , 'Cerro Navia'
            , 'San Jose de Maipo'
            , 'Puente Alto'
            , 'Pirque'
            , 'San Bernardo'
            , 'Calera de Tango'
            , 'Buin'
            , 'Paine'
            , 'Peñaflor'
            , 'Talagante'
            , 'El Monte'
            , 'Isla de Maipo'
            , 'Curacavi'
            , 'Maria Pinto'
            , 'Melipilla'
            , 'San Pedro'
            , 'Alhue'
            , 'Padre Hurtado'],
        "ValorRegion": 13,
        "ValorComunas": [130101,
            130102,
            130103,
            130201,
            130202,
            130203,
            130204,
            130205,
            130206,
            130207,
            130208,
            130209,
            130210,
            130211,
            130212,
            130213,
            130214,
            130215,
            130216,
            130217,
            130218,
            130219,
            130220,
            130221,
            130222,
            130223,
            130224,
            130225,
            130226,
            130227,
            130228,
            130229,
            130230,
            130231,
            130232,
            130301,
            130302,
            130303,
            130401,
            130402,
            130403,
            130404,
            130501,
            130502,
            130503,
            130504,
            130601,
            130602,
            130603,
            130604,
            130605,
            130606]
    },
    {
        "NombreRegion": "Región de Los Ríos",
        "comunas": ['Lanco'
            , 'Mariquina'
            , 'Panguipulli'
            , 'Mafil'
            , 'Valdivia'
            , 'Los Lagos'
            , 'Corral'
            , 'Paillaco'
            , 'Futrono'
            , 'Lago Ranco'
            , 'La Union'
            , 'Rio Bueno'],
        "ValorRegion": 14,
        "ValorComunas": [100101,
            100102,
            100103,
            100104,
            100105,
            100106,
            100107,
            100108,
            100109,
            100110,
            100111,
            100112]
    },
    {
        "NombreRegion": "Región Arica y Parinacota",
        "comunas": ['Gral. Lagos'
            , 'Putre'
            , 'Arica'
            , 'Camarones'],
        "ValorRegion": 15,
        "ValorComunas": [10101,
            10102,
            10201,
            10202]
    },
    {
        "NombreRegion": "Región del Ñuble",
        "comunas": ['Cobquecura'
            , 'Ñiquen'
            , 'San Fabian'
            , 'San Carlos'
            , 'Quirihue'
            , 'Ninhue'
            , 'Trehuaco'
            , 'San Nicolas'
            , 'Coihueco'
            , 'Chillan'
            , 'Portezuelo'
            , 'Pinto'
            , 'Coelemu'
            , 'Bulnes'
            , 'San Ignacio'
            , 'Ranquil'
            , 'Quillon'
            , 'El Carmen'
            , 'Pemuco'
            , 'Yungay'
            , 'Chillan Viejo'],
        "ValorRegion": 16,
        "ValorComunas": [80101,
            80102,
            80103,
            80104,
            80105,
            80106,
            80107,
            80108,
            80109,
            80110,
            80111,
            80112,
            80113,
            80114,
            80115,
            80116,
            80117,
            80118,
            80119,
            80120,
            80121]
    }]
}

jQuery(document).ready(function () {

    var iRegion = 0;
    var htmlRegion = '<option value="0">Seleccione región</option><option value="0">--</option>';
    var htmlComunas = '<option value="0">Seleccione comuna</option><option value="0">--</option>';

    jQuery.each(RegionesYcomunas.regiones, function () {
        htmlRegion = htmlRegion + '<option value="' + RegionesYcomunas.regiones[iRegion].ValorRegion + '">' + RegionesYcomunas.regiones[iRegion].NombreRegion + '</option>';
        iRegion++;
    });

    jQuery('#regiones').html(htmlRegion);
    jQuery('#comunas').html(htmlComunas);

    jQuery('#regiones').change(function () {
        var iRegiones = 0;
        var valorRegion = jQuery(this).val();
        var htmlComuna = '<option value="0">Seleccione comuna</option><option value="0">--</option>';
        jQuery.each(RegionesYcomunas.regiones, function () {
            if (RegionesYcomunas.regiones[iRegiones].ValorRegion == valorRegion) {
                var iComunas = 0;
                jQuery.each(RegionesYcomunas.regiones[iRegiones].comunas, function () {
                    htmlComuna = htmlComuna + '<option value="' + RegionesYcomunas.regiones[iRegiones].ValorComunas[iComunas] + '">' + RegionesYcomunas.regiones[iRegiones].comunas[iComunas] + '</option>';
                    iComunas++;
                });
            }
            iRegiones++;
        });
        jQuery('#comunas').html(htmlComuna);
    });

});