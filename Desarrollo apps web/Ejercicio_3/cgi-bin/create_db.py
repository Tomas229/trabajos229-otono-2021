#!/usr/bin/python3
# -*- coding: utf-8 -*-


import mysql.connector
import os.path

db = mysql.connector.connect(
    host="localhost",
    user="cc500273_u",
    password="pretiumrut",
    database="cc500273_db"
)

cursor = db.cursor()

file = open(os.path.dirname(__file__) + "/../ejercicio3.sql")
operation = file.read()
file.close()

iterator = cursor.execute(operation, multi=True)
iterator.send(None)

print("La tabla se creo con éxito!")
