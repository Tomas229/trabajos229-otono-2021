#!/usr/bin/python3
# -*- coding: utf-8 -*-

from db import Data
import cgi
import cgitb
from utils import imprimir_html

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

form = cgi.FieldStorage()
hbdb = Data("localhost", "cc500273_u", "pretiumrut", "cc500273_db")

hbdb.save_data(form["region"].value, form["comuna"].value, form["sector"].value,
               form["nombre"].value, form["email"].value, form["celular"].value,
               form["dia-hora-avistamiento"], form["tipo-avistamiento"],
               form["estado-avistamiento"], form["foto-avistamiento"], form["numfoto"])

head = """
<meta http-equiv="Refresh" content="0; url='index.py?success=True'"/>
"""
success = """
        <div class="jumbotron main-jumbo">
            <h1 class="display-4">Hemos recibido tu información</h1>
            <p class="lead">Redireccionando</p>
            <p class="lead">
                <a class="btn btn-primary btn-lg" href="../index.html" role="button">Volver al menú</a>
            </p>
        </div>
"""
imprimir_html(success, header=head)
