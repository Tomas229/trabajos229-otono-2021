#!/usr/bin/python3
# -*- coding: utf-8 -*-


from db import Doctor
import cgi
import cgitb

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

utf8stdout = open(1, 'w', encoding='utf-8', closefd=False)

head = """
<!DOCTYPE html>
<html lang="es">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8" /> <!-- Declaring enconding as UTF 8-->
    <title> Ejercicio 3</title> <!-- Title in pestaña -->
    <link rel="stylesheet" type="text/css" media="screen"  href="../style.css" />    <!-- CSS: -->
</head>
"""
body = """
<body>

<ul class="topnav">
  <li><a class="active" href="../index.html">Inicio</a></li>
  <li><a href="../add_doctor.html">Agregar Datos de Médico</a></li>
</ul>


<h1> Los datos fueron enviados con éxito </h1>
"""


footer = """
</body >

"""
form = cgi.FieldStorage()
hbdb = Doctor("localhost", "cc500273_u", "pretiumrut", "cc500273_db")

data = (form["nombre-medico"].value, form["experiencia-medico"].value,
        form["especialidad-medico"].value, form["email-medico"].value, form["celular-medico"].value)

hbdb.save_doctor(data)

print(head, file=utf8stdout)
print(body, file=utf8stdout)
print(footer, file=utf8stdout)
