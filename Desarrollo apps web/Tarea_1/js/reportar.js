function errorRegion() {
    document.getElementsByName("region")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("region")[0].value;
    let textError = "";
    if (text === "sin-region") {
        document.getElementsByName("region")[0].style.borderColor = "red";
        textError += "<p>Se debe seleccionar una región</p>";
    }
    return textError;
}

function errorComuna() {
    document.getElementsByName("comuna")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("comuna")[0].value;
    let textError = "";
    if (text === "sin-comuna") {
        document.getElementsByName("comuna")[0].style.borderColor = "red";
        textError += "<p>Se debe seleccionar una comuna</p>";
    }
    return textError;
}


function errorSector() {
    document.getElementsByName("sector")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("sector")[0].value;
    let textError = "";
    //alert(text.length);
    if (text.length > 100) {
        document.getElementsByName("sector")[0].style.borderColor = "red";
        textError += "<p>El texto de sector no puede superar los 100 carácteres</p>";
    }
    return textError;
}

function errorNombre() {
    document.getElementsByName("nombre")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("nombre")[0].value;
    let textError = "";
    if (text == "") {
        document.getElementsByName("nombre")[0].style.borderColor = "red";
        textError += "<p>Se debe escribir un nombre</p>";
    }
    if (text.length > 200) {
        document.getElementsByName("nombre")[0].style.borderColor = "red";
        textError += "<p>El nombre no puede superar los 200 carácteres</p>";
    }
    let regex = /^[a-zA-ZÀ-ÿ\u00f1\u00d1 ]*$/; //aceptar ñ y acentos
    if (!regex.test(text)) {
        document.getElementsByName("nombre")[0].style.borderColor = "red";
        textError += "<p>El nombre solamente puede estar compuesto por letras</p>";
    }
    return textError;
}

function errorEmail() {
    document.getElementsByName("email")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("email")[0].value;
    let textError = "";
    if (text == "") {
        document.getElementsByName("email")[0].style.borderColor = "red";
        textError += "<p>Se debe escribir un email</p>";
    }
    let regex = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
    if (!regex.test(text)) {
        document.getElementsByName("email")[0].style.borderColor = "red";
        textError += "<p>El email tiene un formato inválido</p>";
    }
    return textError;
}

function errorCelular() {
    document.getElementsByName("celular")[0].style.borderColor = "#ced4da"
    let text = document.getElementsByName("celular")[0].value;
    let textError = "";
    let regex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
    if (!regex.test(text)) {
        document.getElementsByName("celular")[0].style.borderColor = "red";
        textError += "<p>El celular tiene un formato inválido</p>";
    }
    return textError;
}

function errorDiaHora() {
    let textError = "";
    let listDiaHora = document.getElementsByName("dia-hora-avistamiento");
    listDiaHora.forEach(function (currentValue, currentIndex) {
        currentValue.style.borderColor = "#ced4da"
        if (currentValue.value === "") {
            currentValue.style.borderColor = "red";
            textError += "<p>Se debe escribir un día y hora para el avistamiento " + String(currentIndex + 1) + " </p>";
        } else {
            let regex = /[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]) (2[0-3]|[01][0-9]):[0-5][0-9]/;
            if (!regex.test(currentValue.value)) {
                currentValue.style.borderColor = "red";
                textError += "<p>El día y hora del avistamiento " + String(currentIndex + 1) + "no tiene formato aaaa-mm-dd hh:mm</p>";
            }
        }
    }
    );
    return textError;
}

function errorTipo() {
    let textError = "";
    let listTipo = document.getElementsByName("tipo-avistamiento");
    listTipo.forEach(function (currentValue, currentIndex) {
        currentValue.style.borderColor = "#ced4da"
        if (currentValue.value === "sin-tipo") {
            currentValue.style.borderColor = "red";
            textError += "<p>Se debe seleccionar un tipo para el avistamiento " + String(currentIndex + 1) + " </p>";
        }
    }
    );
    return textError;
}

function errorEstado() {
    let textError = "";
    let listEstado = document.getElementsByName("estado-avistamiento");
    listEstado.forEach(function (currentValue, currentIndex) {
        currentValue.style.borderColor = "#ced4da"
        if (currentValue.value === "sin-estado") {
            currentValue.style.borderColor = "red";
            textError += "<p>Se debe seleccionar un estado para el avistamiento " + String(currentIndex + 1) + " </p>";
        }
    }
    );
    return textError;
}

function errorFoto() {
    let images = document.getElementsByName("foto-avistamiento");
    let textError = "";

    let list = [[]];
    let index = 0;
    images.forEach(
        function (currentValue) {
            let id = currentValue.parentNode.parentNode.id;
            id = id[id.length - 1];
            if (index != id) {
                index += 1;
                list.push([]);
            }
            list[index].push(currentValue.value);
        }
    );
    //Verify if there's at least one file uploaded
    for (j = 0; j < list.length; j++) {
        atLeastOne = false;
        for (h = 0; h < list[j].length; h++) {
            if (list[j][h] != "") {
                atLeastOne = true;
            }
        }
        if (!atLeastOne) {
            textError += "<p>Se debe seleccionar al menos una foto del avistamiento " + String(j + 1) + "</p>";
        } else {
            //If there is at least one file, verify if it's an image
            let regex = /(\.jpg|\.jpeg|\.png)$/;
            for (h = 0; h < list[j].length; h++) {
                if (list[j][h] != "" && !regex.test(list[j][h])) {
                    textError += "<p>El archivo subido número " + String(h + 1) + " del avistamiento " + String(j + 1) + " no es una imagen</p>"
                }
            }
        }
    }
    return textError
}

function errorVerifier() {
    let textError = ""
    textError += errorRegion();
    textError += errorComuna();
    textError += errorSector();
    textError += errorNombre();
    textError += errorEmail();
    textError += errorCelular();
    textError += errorDiaHora();
    textError += errorTipo();
    textError += errorEstado();
    textError += errorFoto();

    let alertMessage = document.getElementById("userErrors");
    if (textError !== "") {
        alertMessage.innerHTML = textError;
        alertMessage.style.display = "block";
        $('#sendModal').modal('hide');
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        document.body.scrollTop = 0; // For Safari
        return false;
    }
    return true;
}

function getDateFormat() {
    let date = new Date();
    let year = date.getFullYear();
    let month = String(date.getMonth() + 1).padStart(2, '0');
    let day = String(date.getDate()).padStart(2, '0');
    let hour = String(date.getHours() + 1).padStart(2, '0');
    let minute = String(date.getMinutes() + 1).padStart(2, '0');

    return year + '-' + month + '-' + day + ' ' + hour + ':' + minute
}

function anotherFile(n) {
    let number = 0
    document.getElementsByName("foto-avistamiento").forEach(
        function (currentValue) {
            if (currentValue.parentNode.parentNode.id == "image-buttons" + String(n)) {
                number += 1;
            }
        }
    );
    if (number < 5) {
        let c = document.getElementById("image-buttons" + String(n));
        c.insertAdjacentHTML("beforeend", '<div class="col-sm-6"><input type = "file" class="form-control-file" name = "foto-avistamiento"></div>');
    } else {
        alert("No se pueden subir más fotos");
    }
}

let numPhotos = 0;
function anotherReport() {
    numPhotos += 1;
    let c = document.getElementById("info-avistamiento");
    c.insertAdjacentHTML("beforeend",
        `
    <div class="text-center">
    <b>Información avistamiento `+ String(numPhotos + 1) + `:</b>
    </div>

    <div class="form-group row">
        <label for="inputFecha" class="col-sm-2 col-form-label">Día hora:</label>
        <div class="col-sm-10">
            <input type="text" size="20" class="form-control" name="dia-hora-avistamiento"
                placeholder="aaaa-mm-dd hh:mm">
        </div>
    </div>

    <div class="form-group row">
        <label for="inputTipo" class="col-sm-2 col-form-label">Tipo:</label>
        <div class="col-sm-10">
            <select class="custom-select" name="tipo-avistamiento">
                <option selected value="sin-tipo">Seleccione el tipo del avistamiento</option>
                <option value="nose">No sé</option>
                <option value="insecto">Insecto</option>
                <option value="aracnido">Arácnido</option>
                <option value="miriapodo">Miriápodo</option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEstado" class="col-sm-2 col-form-label">Estado:</label>
        <div class="col-sm-10">
            <select class="custom-select" name="estado-avistamiento">
                <option selected value="sin-estado">Seleccione un estado para el avistamiento</option>
                <option value="nose">No sé</option>
                <option value="vivo">Vivo</option>
                <option value="muerto">Muerto</option>
            </select>
        </div>
    </div>
    ` + `
    <div class="form-group row" id="image-buttons`+ String(numPhotos) + `">
        <label for="inputFoto" class="col-sm-2 col-form-label">Foto:</label>
        <div class="col-sm-6">
            <input type="file" class="form-control-file" name="foto-avistamiento">
        </div>
        <div class="col-sm-4">
            <button class="btn-success text-center" role="button" type="button" onclick="anotherFile(`+ String(numPhotos) + `)">
                                Agregar otra foto
            </button>
        </div>
    </div>
    <hr class="my-4">
    `);
    let diaHoraForm = document.getElementsByName("dia-hora-avistamiento")
    diaHoraForm[diaHoraForm.length - 1].value = getDateFormat();
}

function submitModal() {
    const params = new URLSearchParams(document.location.search);
    if (params.get("var1") === "true") {
        $('#submitModal').modal('toggle')
    }
}

function noConstraint() {
    document.getElementsByName("sector")[0].removeAttribute('maxLength');

    document.getElementsByName("nombre")[0].required = false;
    document.getElementsByName("nombre")[0].removeAttribute('maxLength');

    document.getElementsByName("email")[0].required = false;

    // for
    document.getElementsByName("dia-hora-avistamiento").forEach(function (currentValue) {
        currentValue.required = false;
    });
}
