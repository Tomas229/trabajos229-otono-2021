#!/usr/bin/python3
# -*- coding: utf-8 -*-

from db import Data
import cgi
import cgitb
from utils import imprimir_html
import html

cgitb.enable()

print("Content-type:text/html\r\n\r\n")

hbdb = Data("localhost", "cc500273_u", "pretiumrut", "cc500273_db")

form = cgi.FieldStorage()

if not form:
    page = 0
else:
    page = max(0, int(form["page"].value))

data = hbdb.get_list(page*5)

success = ""

if data:
    success += """
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Fecha-Hora</th>
                    <th scope="col">Comuna</th>
                    <th scope="col">Sector</th>
                    <th scope="col">Nombre contacto</th>
                    <th scope="col">Total avistamientos</th>
                    <th scope="col">Total fotos</th>
                </tr>
            </thead>
            <tbody>
    """
    for d in data:
        count_da = hbdb.get_list_count_da(d[0])
        count_fo = hbdb.get_list_count_fo(d[0])

        url_data = "data.py?id=" + html.escape(str(d[0]))
        success += """
        <tr onClick="location.href='""" + url_data + """'
        " class="my-row">"""
        for i in range(1, len(d)):
            success += f"<td>{html.escape(str(d[i]))}</td>"
        success += f"<td>{html.escape(str(count_da[0][0]))}</td>"
        success += f"<td>{html.escape(str(count_fo[0][0]))}</td>"
        success += "</tr>"

    success += """
            </tbody>
        </table>
    </div>
    """
    last_page = hbdb.get_av_count()[0][0]
    success += """
    <div class="my-pagination">
        <nav aria-label="Páginas de avistamientos">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" href="list.py?page=""" + str(0) + """ " aria-label="First">
                        <span aria-hidden="true">&laquo;&laquo;</span>
                        <span class="sr-only">First</span>
                    </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="list.py?page=""" + str(max(0, page - 1)) + """ " aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item active">
                    <span class="page-link">
                        """ + str(page + 1) + """
                        <span class="sr-only">(current)</span>
                    </span>
                </li>
                <li class="page-item">
                    <a class="page-link" href="list.py?page=""" + str(page + 1) + """ " aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
                <li class="page-item">
                    <a class="page-link" href="list.py?page=""" + str((last_page-1)//5) + """ " aria-label="Last">
                        <span aria-hidden="true">&raquo;&raquo;</span>
                        <span class="sr-only">Last</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    """

    imprimir_html(success)

else:
    success += """
    <h1 class= "text-center">No se encontraron datos</h1>
    <button type="button" class="btn-lg btn-secondary btn-block text-center" onclick="goBack()">
        Volver
    </button>
    """

    imprimir_html(success, '<script src="../js/list.js"></script>')
