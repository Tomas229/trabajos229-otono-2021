#!/usr/bin/python3
# -*- coding: utf-8 -*-

from db import Data
import cgi
import cgitb
from utils import imprimir_html, imprimir_form

cgitb.enable()

print("Content-type:text/html\r\n\r\n", flush=True)

# TODO arreglar js de la fecha 19:60
form = cgi.FieldStorage()

if len(form) > 0:
    hbdb = Data("localhost", "cc500273_u", "pretiumrut", "cc500273_db")

    errors = hbdb.save_data(form["region"].value, form["comuna"].value, form["sector"].value,
                            form["nombre"].value, form["email"].value, form["celular"].value,
                            form["dia-hora-avistamiento"], form["tipo-avistamiento"],
                            form["estado-avistamiento"], form["foto-avistamiento"], form["numfoto"])
    if errors:
        errors = f"""
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
        {errors}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        """
        imprimir_form(errors)
    else:
        head = """
        <meta http-equiv="Refresh" content="0; url='index.py?success=True'"/>
        """
        success = """
                <div class="jumbotron main-jumbo">
                    <h1 class="display-4">Hemos recibido tu información</h1>
                    <p class="lead">Redireccionando</p>
                    <p class="lead">
                        <a class="btn btn-primary btn-lg" href="../index.html" role="button">Volver al menú</a>
                    </p>
                </div>
                """
        imprimir_html(success, header=head)
else:
    imprimir_form()
