#!/usr/bin/python3
# -*- coding: utf-8 -*-

import mysql.connector
import hashlib
import os
import filetype
from utils import imprimir_error
MAX_FILE_SIZE = 10000 * 1000  # 10 MB


class Doctor:

    def __init__(self, host, user, password, database):
        self.db = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )
        self.cursor = self.db.cursor()

    def save_doctor(self, data):
        # procesar y guardar el archivo
        fileobj = data[5]
        filename = fileobj.filename

        if not filename:
            imprimir_error('Archivo no subido')

        # verificamos el tipo
        size = os.fstat(fileobj.file.fileno()).st_size
        if size > MAX_FILE_SIZE:
            imprimir_error('Tamaño excede 10mb')

        # calculamos cuantos elementos existen y actualizamos el hash
        sql = "SELECT COUNT(id) FROM archivo"
        self.cursor.execute(sql)
        total = self.cursor.fetchall()[0][0] + 1  # peligroso
        hash_archivo = str(total) + \
            hashlib.sha256(filename.encode()).hexdigest()[0:30]

        # guardar el archivo
        file_path = '../img/' + hash_archivo
        open(file_path, 'wb').write(fileobj.file.read())

        # verificamos el tipo, si no es valido lo borramos de la db
        tipo = filetype.guess(file_path)
        if tipo.mime.split("/")[0] != 'image':
            os.remove(file_path)
            imprimir_error('Tipo archivo no es imagen')

        # guardamos la imagen en la db
        sql = """
            INSERT INTO archivo (ruta_archivo, nombre_archivo)
            VALUES (%s, %s)
        """
        self.cursor.execute(sql, (hash_archivo, filename))
        self.db.commit()  # id
        id_archivo = self.cursor.getlastrowid()

        # Se guarda doctor en la db
        sql = '''
            INSERT INTO medico (nombre, experiencia, especialidad, email, celular, archivo_id)
            VALUES (%s, %s, %s, %s, %s, %s)
        '''
        self.cursor.execute(
            sql, (*data[0:-1], id_archivo))  # ejecuto la consulta
        self.db.commit()  # modifico la base de datos

    def get_doctors(self):
        sql = f"""
            SELECT * FROM medico
        """
        self.cursor.execute(sql)
        return self.cursor.fetchall()  # retornamos la data

    def get_archivo_from_id(self, id):
        sql = f"""
        SELECT * FROM archivo WHERE id = {id}
        """
        self.cursor.execute(sql, id)
        return self.cursor.fetchall()
